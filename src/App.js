import React, {
  Component
} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

class App extends Component {

  constructor(props){
    super();
    this.state = {
      session :"",
      token : "",
      isLoading: false,
      error: null,
      };
  }


  async componentDidMount(){
    this.setState({isLoading:true})

    const data = {
      "secret": "your_secret",
      "merchantId": "your_merchantID",
      "terminal": "termianlID",
      "amount": 500,
      "currency": "currency_code [USD,CRC]",
      "desciption": "some_description",
      "orderReference": "unique_order_ID"
    }

    axios.post("https://sandbox-merchant.greenpay.me",data,
      {
        headers : {
        'Accept': 'application/json',
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
        }
      }).then((response)=>
      {
        // console.log('response', response.data)
        this.setState({
          session:response.data.session,
          token: response.data.token,
          isLoading:false
        })
    }).catch((error)=>{
      alert("There is an error in API call.");
      this.setState({
        error,
        isLoading:false
      })
    })
  }


  render() {
    const {session, token, isLoading} = this.state;

    // console.log(security);

    if(isLoading){
      return <h2>Loading ...</h2>
    }
    // console.log('state',this.setState)
    // console.log('token',session);
    return (
    <div className = "App" >
        <div gpsession = {session} gptoken = {token}> </div>
    </div >
    );
  }
}

export default App